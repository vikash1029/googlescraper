<?php
	require('../Dal/Search.php');
	require('../Dal/SearchResults.php');
	require ('../vendor/autoload.php');
	require('../Dal/DBConnect.php');
	require('../Dal/SqlQueries.php');
	Logger::configure('../Logconfig.xml');
	// $logger->info('info vikash');
	// $logger->error('error ');
	// $logger->warn('warn ');
	// $logger->fatal('fatal'); 
	// $logger->debug('debug '); //not returning result
	// $logger->trace('trace '); //not returning result

	class GoogleSearch {
	  public function __construct(logger $logger) {  
	  	$this->logger = $logger;
	  }
		public function getResult() {	
			if (isset($_POST['search'])) {
				try {
					$searchTxt = $_POST['search'];
					$dbSearchObj = new Search();
					$dbSearchData = $dbSearchObj->select($searchTxt);
					$dbSearchResultsObj = new SearchResults();  

					if (empty($dbSearchData)) {		
						$scrape = $this->doGoogleSearch($searchTxt);
					  	$results = $this->parseHtml($scrape);
						$htmlResults = $this->createResultObject($results);
						$insertId = $dbSearchObj->insert($searchTxt);
						foreach ($htmlResults as $save) {
							$dbSearchResultsObj->insert($insertId, $save['anchorText'], $save['anchorValue']);	
						}
					} else {
						$htmlResults = [];
						foreach ($dbSearchData as $data) {
							$htmlResults[] = ['anchorText' =>$data['anchorText'], 'anchorValue' => $data['anchorValue']];
						}
				  }	
				} catch (Exception $e) {
					$this->logger->error(sprintf('Error in GoogleSearch.getResult Message is %s. Stack Trace is %s',$e->getMessage(), $e->getTraceAsString()));
				}
			  echo json_encode($htmlResults);
			}
		}
		private function doGoogleSearch(string $searchTxt):array {
			$url = 'http://www.google.co.in/search?q='.urlencode($searchTxt).'';
			$scrape[] = file_get_contents($url);
			return $scrape;
		}

		private function parseHtml(array $scrape):object {
			$doc = new DOMDocument();
			@$doc->loadHTML($scrape[0]);
			$xpath = new DOMXpath($doc);
			$results = $xpath->query("//*[contains(@class, 'r')]");
			return $results;
		}

		private function createResultObject(object $results):array {
			$htmlResults = [];
			foreach($results as $container) {
				$attribute = $container->getElementsByTagName('a');				
				foreach ($attribute as $link) {
					if (count($htmlResults) == 10) {
						break;
					}
					$href = $link->getAttribute("href");
					$text = trim(preg_replace("/[\r\n]+/", " ", $link->nodeValue));
					if (strchr($href, '/url?q=https://') && $text != '') {
						$href = str_replace('/url?q=', '', $href);
						$htmlResults[] =['anchorValue' => $href, 'anchorText' => $text]; 
					}
				}
			}
			return $htmlResults;
		}
	}
	$logger = Logger::getLogger('GoogleScraper');
	$search = new GoogleSearch($logger);
	$search->getResult();
?>