<?php

	class Search {
		private $logger;
		private $queryObj;
		public function __construct(){
			$this->logger = Logger::getLogger('Search');
			$this->queryObj = new SqlQueries;
		}
		public function select(string $title): array {
			if (empty($title)) {
				throw new InvalidArgumentException("Title cannot be empty");			
			}
			try {
				$query = "SELECT * FROM search INNER JOIN results  ON search.id = results.searchId WHERE title = :title ";
				$rslt = $this->queryObj->executeSql($query, array(':title'=>$title));
				return $rslt;
			} catch(Exception $e) {
				$this->logger->error(sprintf('Error in Search.select  Message is %s. Stack Trace is %s',$e->getMessage(), $e->getTraceAsString()));
			}
		}

		public function insert(string $title): int {

			if (empty($title)) {
				throw new InvalidArgumentException("Title Should Not be empty");
			}
			try {
				$query = "INSERT INTO search SET title=:title";
				$result = $this->queryObj->insertAndReturnLastId($query, array(':title'=>$title));
				return $result;	
			} catch(Exception $e) {
				$this->logger->error(sprintf('Error in Search.insert Message is %s. Stack Trace is %s',$e->getMessage(), $e->getTraceAsString()));
			}
		}
	}
?>