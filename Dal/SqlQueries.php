<?php
    class SqlQueries {
        private $logger;
        private $conn;
        public function __construct() {
            $this->logger = Logger::getLogger('SqlQueries');
            $this->dbConnect = new DBConnect;
        }
        private function runQuery (string $query):object {
            $this->conn = $this->dbConnect->getConnection();
            $sql = $this->conn->prepare($query);
            return $sql;
        }


        public function executeSql(string $query, Array $parameters):array {
            try {
                $sql = $this->runQuery($query);
                $sql->execute($parameters);
                return $sql->fetchAll();
            } catch (Exception $e) {
                $this->logger->error(sprintf('Error in SqlQueries.executeSql Message is %s. Stack Trace is %s',$e->getMessage(), $e->getTraceAsString()));
            }
        }

        public function insertAndReturnLastId(string $query, Array $parameters): int {
            try {
                $sql = $this->runQuery($query);
                $sql->execute($parameters);
                $lastInsertId = $this->conn->lastInsertId();
                return $lastInsertId;
            } catch (Exception $e) {
                $this->logger->error(sprintf('Error in SqlQueries.executeSql Message is %s. Stack Trace is %s',$e->getMessage(), $e->getTraceAsString()));
            }
        }
        
    }
?>