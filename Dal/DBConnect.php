<?php
require('../Const.php');

class DBConnect {
	private $logger;
	public function __construct() {
		$this->logger = Logger::getLogger('GoogleScraper');
	}
	public function getConnection(): PDO {
		try {
			$conn = new PDO("mysql:host=".HOST_NAME.";dbname=".DATABASE.";charset=utf8", USER_NAME, PASSWORD);
			return $conn;
		}	catch (PDOException $e) {
				$this->logger->error(sprintf('Error in DBConnect. Message is %s. Stack Trace is %s',$e->getMessage(), $e->getTraceAsString()));
			}
	} 
}			 
?>
