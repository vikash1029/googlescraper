<?php
	
	class SearchResults {
		private $logger;
		private $queryObj;
		public function __construct() {
			$this->logger = Logger::getLogger('SearchResults');
			$this->queryObj = new SqlQueries;
		}

		public function insert(int $lastId,string $anchorText,string $anchorValue): bool {			
			if (is_int($lastId) === false && $lastId > 0) {
				throw new InvalidArgumentException("Last id should not be empty");
			}

			if (empty($anchorText)) {
				throw new InvalidArgumentException("Anchor Text Should not be empty");	
			}

			if (empty($anchorValue)) {
				throw new InvalidArgumentException("Anchor Value Should not be empty");	
			}

			try {
				$query = "INSERT INTO results SET searchId=:lastId, anchorText=:anchorText, anchorValue=:anchorValue";
				$data = array(':lastId'=>$lastId, ':anchorText'=>$anchorText, ':anchorValue'=>$anchorValue);
				$result = $this->queryObj->insertAndReturnLastId($query, $data);
				return true;		
			} catch(Exception $e) {
				$this->logger->error(sprintf('Error in SearchResults.insert Message is %s. Stack Trace is %s',$e->getMessage(), $e->getTraceAsString()));				
			}
		}	
	}
?>